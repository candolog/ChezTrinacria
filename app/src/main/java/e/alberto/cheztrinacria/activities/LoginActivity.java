package e.alberto.cheztrinacria.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import e.alberto.cheztrinacria.data.JSONCommunication;
import e.alberto.cheztrinacria.R;
import e.alberto.cheztrinacria.utils.Utils;

public class LoginActivity extends AppCompatActivity {

    private EditText user;
    private EditText pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        user = findViewById(R.id.username);
        pass = findViewById(R.id.password);
    }

    /**
     * Sends to the server a request for log in.
     *
     * @param view
     * @throws IOException
     * @throws JSONException
     */
    public void login(View view) throws IOException, JSONException {

        final String username = user.getText().toString().trim();
        String password = pass.getText().toString();

        if(!Utils.isValidEmail(username)) {
            Toast.makeText(getApplicationContext(), "E-mail invalid.", Toast.LENGTH_SHORT).show();
        }

        JSONObject loginObject = new JSONObject();
        loginObject.put("email", username);
        loginObject.put("password", password);
        JSONCommunication.getInstance().jsonObjectRequest(
                "user/login",
                LoginActivity.this,
                Request.Method.POST,
                loginObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getBoolean("response")){
                                if (!LoginActivity.this.isFinishing()) {
                                    Utils.makeShortToast(LoginActivity.this, "Login successfull.");
                                    SharedPreferences loginSharedPreferences = LoginActivity.this.getSharedPreferences("loginPreferences", MODE_PRIVATE);
                                    loginSharedPreferences.edit().putBoolean("isUserLogged", true).apply();
                                    loginSharedPreferences.edit().putString("username", username).apply();
                                    Utils.goToActivity(LoginActivity.this, MainActivity.class);
                                    finish();
                                }
                                else Utils.makeShortToast(LoginActivity.this, response.getString("msg"));
                            }
                            else Utils.makeShortToast(LoginActivity.this, response.getString("msg"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.makeShortToast(LoginActivity.this, "Email or password wrong.");
                    }
                }
        );
    }

    /**
     * Recalls RegisterActivity.
     *
     * @param view
     */
    public void goToRegisterPage(View view) {
        Utils.goToActivity(LoginActivity.this, RegisterActivity.class);
    }

    @Override
    public void onBackPressed(){
        Intent home = new Intent(Intent.ACTION_MAIN);
        home.addCategory(Intent.CATEGORY_HOME);
        home.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(home);
    }
}