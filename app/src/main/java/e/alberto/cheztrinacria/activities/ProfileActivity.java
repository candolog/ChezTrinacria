package e.alberto.cheztrinacria.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import e.alberto.cheztrinacria.R;
import e.alberto.cheztrinacria.data.JSONCommunication;
import e.alberto.cheztrinacria.utils.Utils;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        getProfileInformations();
    }

    /**
     * Gets information about the user logged in.
     */
    private void getProfileInformations() {
        /*
        We check if the user is logged in, otherwise we set null.
         */
        SharedPreferences loginSharedPreferences = ProfileActivity.this.getSharedPreferences("loginPreferences", MODE_PRIVATE);
        String username = loginSharedPreferences.getString("username", null);
        /*
        URL for the API
         */
        username = "user/info/" + username;

        JSONCommunication.getInstance().jsonObjectRequest(
                username,
                ProfileActivity.this,
                Request.Method.GET,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            TextView email = findViewById(R.id.mail_);
                            TextView name = findViewById(R.id.name_);
                            TextView address = findViewById(R.id.address_);
                            TextView telephone = findViewById(R.id.telephone_);
                            TextView points = findViewById(R.id.points_);
                            email.setText("Email: " + response.getString("Email"));
                            address.setText("Address: " + response.getString("Address"));
                            name.setText("Name: " + response.getString("Name") + " " + response.getString("Surname"));
                            telephone.setText("Telephone: " + response.getString("Telephone"));
                            points.setText("Points: " + String.valueOf(response.getInt("Points")));
                        } catch (JSONException e) {
                            /*
                             If we are here then the URL was wrong, which means thar the user didn't logged in.
                             */
                            Utils.makeShortToast(ProfileActivity.this, "You must be logged in.");
                            Utils.goToActivity(ProfileActivity.this, LoginActivity.class);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ProfileActivity.this, "Failed to get the information.", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    /**
     * Permits the user to log out.
     *
     * @param v
     */
    public void logout (View v) {
        SharedPreferences loginPreferences = ProfileActivity.this.getSharedPreferences("loginPreferences", MODE_PRIVATE);
        loginPreferences.edit().putBoolean("isUserLogged", false).apply();
        Utils.makeShortToast(ProfileActivity.this, "Logout successfull.");
        Utils.goToActivity(ProfileActivity.this, LoginActivity.class);
        finish();
    }

    /**
     * Starts ChangeInformation
     *
     * @param v
     */
    public void change_info(View v){
        TextView address = findViewById(R.id.address_);
        Intent edit_page = new Intent(this, ChangeInformation.class);
        String a = (String) address.getText();
        edit_page.putExtra("address", a.substring(8));
        startActivity(edit_page);
    }

    @Override
    public void onResume(){
        super.onResume();
        getProfileInformations();
    }

    @Override
    public void onBackPressed(){
        Utils.goToActivity(ProfileActivity.this, MainActivity.class);
    }
}