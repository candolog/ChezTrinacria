package e.alberto.cheztrinacria.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import e.alberto.cheztrinacria.R;
import e.alberto.cheztrinacria.data.Course;
import e.alberto.cheztrinacria.data.GPSActivity;
import e.alberto.cheztrinacria.utils.HashMapAdapter;
import e.alberto.cheztrinacria.utils.Utils;

public class OrderActivity extends AppCompatActivity{

    ArrayList<Course> piatti;
    private int content = 0;
    private String address = "";
    float price = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order);
        piatti = getIntent().getParcelableArrayListExtra("list");
        //We use an HashMap to display a summary of what the user is ordering.
        HashMap<String, Integer> map = new HashMap<>();
        for(int i = 0; i < piatti.size(); i++)
        {
            map.put(piatti.get(i).getName(), piatti.get(i).getQuantity());
            price += (piatti.get(i).getPrice())*piatti.get(i).getQuantity();
        }
        HashMapAdapter panier_adapter = new HashMapAdapter(map);
        ListView panierView = findViewById(R.id.panier_summary);
        panierView.setAdapter(panier_adapter);
        TextView totalprice = findViewById(R.id.total_price);
        totalprice.setText(String.format("Total: %.2f€",price));
    }

    public void confirm_order (View v){
        EditText notes = findViewById(R.id.notes);
        Intent confirmorder = new Intent(this, ConfirmOrderActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("notes", String.valueOf(notes.getText()));
        bundle.putParcelableArrayList("list", piatti);
        bundle.putDouble("total", price);
        bundle.putString("address", address);
        confirmorder.putExtras(bundle);
        startActivity(confirmorder);
    }

    public void start_gps(View v){
        Utils.goToActivity(OrderActivity.this, GPSActivity.class);
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences address_ = OrderActivity.this.getSharedPreferences("addr", MODE_PRIVATE);
        address = address_.getString("address", "-");
    }
}
