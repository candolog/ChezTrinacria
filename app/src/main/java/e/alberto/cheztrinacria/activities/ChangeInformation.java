package e.alberto.cheztrinacria.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import e.alberto.cheztrinacria.R;
import e.alberto.cheztrinacria.data.GPSActivity;
import e.alberto.cheztrinacria.data.JSONCommunication;
import e.alberto.cheztrinacria.utils.Utils;

public class ChangeInformation extends AppCompatActivity{

    private EditText address;
    private EditText name;
    private EditText surname;
    private EditText password;
    private EditText telephone;
    private String username;
    private String default_address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_info_layout);
        address = findViewById(R.id.address_change);
        default_address = getIntent().getStringExtra("address");
        SharedPreferences address_ = ChangeInformation.this.getSharedPreferences("addr", MODE_PRIVATE);
        address_.edit().putString("address", default_address).apply();
        address.setText(address_.getString("address", default_address));
        surname = findViewById(R.id.surname_change);
        name = findViewById(R.id.name_change);
        password = findViewById(R.id.password_change);
        telephone = findViewById(R.id.mobile_change);
        //Controllo se l'utente ha già effettuato il login, in caso contrario assegno null
        /**
         * Checks if the user is logged in, otherwise we set 'username' to null.
         */
        SharedPreferences loginSharedPreferences = ChangeInformation.this.getSharedPreferences("loginPreferences", MODE_PRIVATE);
        username = loginSharedPreferences.getString("username", null);
        getProfileInformations();
    }

    /**
     * Updates on the DB the new personal information that the user set.
     *
     * @param v
     * @throws JSONException
     */
    public void save_new_info(View v) throws JSONException {
        JSONObject js = new JSONObject();
        js.put("Email", username);
        js.put("Name", name.getText());
        js.put("Surname", surname.getText());
        js.put("Telephone", telephone.getText());
        js.put("Password", password.getText());
        js.put("Address", address.getText());
        JSONCommunication.getInstance().jsonObjectRequest(
                "user/update",
                ChangeInformation.this,
                Request.Method.POST,
                js,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!ChangeInformation.this.isFinishing()) {
                                if(response.getBoolean("response")) {
                                    Utils.makeShortToast(getApplicationContext(), "Changing information successfully.");
                                    Utils.goToActivity(ChangeInformation.this, ProfileActivity.class);
                                }
                                else {
                                    Utils.makeShortToast(ChangeInformation.this, response.getString("msg"));
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (!ChangeInformation.this.isFinishing()) {
                            Utils.makeShortToast(ChangeInformation.this, "Failed modifications. Check your internet connection.");
                        }
                    }
                }
        );
    }

    /**
     * Starts GPSActivity.
     *
     * @param v
     */
    public void start_gps_(View v){
        Utils.goToActivity(ChangeInformation.this, GPSActivity.class);
    }

    /**
     *
     */
    private void getProfileInformations() {
        /*
        Here we create the URL for the API
         */
        String api = "user/info/" + username;

        JSONCommunication.getInstance().jsonObjectRequest(
                api,
                ChangeInformation.this,
                Request.Method.GET,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            name.setText(response.getString("Name"));
                            surname.setText(response.getString("Surname"));
                            telephone.setText(response.getString("Telephone"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ChangeInformation.this, "Failed to get actual the information.", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    @Override
    public void onResume()
    {
        super.onResume();
        SharedPreferences address_ = ChangeInformation.this.getSharedPreferences("addr", MODE_PRIVATE);
        address.setText(address_.getString("address", default_address));
    }
}