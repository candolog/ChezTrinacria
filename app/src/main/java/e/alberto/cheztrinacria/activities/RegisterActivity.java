package e.alberto.cheztrinacria.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import e.alberto.cheztrinacria.data.GPSActivity;
import e.alberto.cheztrinacria.data.JSONCommunication;
import e.alberto.cheztrinacria.R;
import e.alberto.cheztrinacria.utils.Utils;

public class RegisterActivity extends AppCompatActivity{
    private EditText surname;
    private EditText name;
    private EditText mobile;
    private EditText mail;
    private EditText address;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        Utils.getSharedAddress(RegisterActivity.this);

        surname = findViewById(R.id.surname);
        name = findViewById(R.id.name);
        mobile = findViewById(R.id.mobile);
        mail = findViewById(R.id.email);
        password = findViewById(R.id.password);
    }

    /**
     * Registers a new user
     *
     * @param v
     * @throws JSONException
     */
    public void register (View v) throws JSONException {
        if(!Utils.isValidEmail(mail.getText())) {
            Toast.makeText(getApplicationContext(), "E-mail invalid.", Toast.LENGTH_SHORT).show();
        } else {
            final JSONObject registerObject = new JSONObject();
            registerObject.put("surname", surname.getText().toString());
            registerObject.put("name", name.getText().toString());
            registerObject.put("telephone", mobile.getText().toString());
            registerObject.put("email", mail.getText().toString());
            registerObject.put("password", password.getText().toString());
            registerObject.put("address", address.getText().toString());
            JSONCommunication.getInstance().jsonObjectRequest(
                    "user/register",
                    RegisterActivity.this,
                    Request.Method.POST,
                    registerObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (!RegisterActivity.this.isFinishing()) {
                                    if(response.getBoolean("response")) {
                                        SharedPreferences loginSharedPreferences = RegisterActivity.this.getSharedPreferences("loginPreferences", MODE_PRIVATE);
                                        loginSharedPreferences.edit().putBoolean("isUserLogged", true).apply();
                                        loginSharedPreferences.edit().putString("username",registerObject.getString("email")).apply();
                                        Utils.makeShortToast(getApplicationContext(), "Registration successfully.");
                                        Utils.goToActivity(RegisterActivity.this, MainActivity.class);
                                    }
                                    else {
                                        Utils.makeShortToast(RegisterActivity.this, response.getString("msg"));
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (!RegisterActivity.this.isFinishing()) {
                                Utils.makeShortToast(RegisterActivity.this, "Registration failed. Check your internet connection.");
                            }
                        }
                    }
            );
        }
    }

    /**
     * Starts GPSActivity.
     *
     * @param v the View that recalls the method start_gps_()
     */
    public void start_gps_(View v){
        Utils.goToActivity(RegisterActivity.this, GPSActivity.class);
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences address_ = RegisterActivity.this.getSharedPreferences("addr", MODE_PRIVATE);
        address = findViewById(R.id.address_register);
        address.setText(address_.getString("address", "-"));
    }
}
