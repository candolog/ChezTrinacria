package e.alberto.cheztrinacria.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import e.alberto.cheztrinacria.R;
import e.alberto.cheztrinacria.utils.Utils;

public class LateralBarActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        int choose = i.getIntExtra("choose", 0);
        setLayout(choose);
    }

    /**
     * Displays a new layout based on the value of choose.
     *
     * @param choose An integer that represents the choose.
     */
    private void setLayout (int choose){
        if(choose == 4) {
            setContentView(R.layout.credits);
            /*
            Logo animation
             */
            ImageView logo = findViewById(R.id.logo);
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation);
            logo.startAnimation(animation);
        } else if(choose == 1) {
            setContentView(R.layout.profile);
        } else if(choose == 2) {
            setContentView(R.layout.points_functioning);
        }
    }
}
