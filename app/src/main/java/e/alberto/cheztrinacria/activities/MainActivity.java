package e.alberto.cheztrinacria.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import e.alberto.cheztrinacria.R;
import e.alberto.cheztrinacria.data.Course;
import e.alberto.cheztrinacria.data.JSONCommunication;
import e.alberto.cheztrinacria.utils.HashMapAdapter;
import e.alberto.cheztrinacria.utils.MenuItemAdapter;
import e.alberto.cheztrinacria.utils.Utils;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Map<String, Integer> panier = new HashMap<>();
    ArrayList<Course> piatti;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Graphics settings routine
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        lockGoToOrder();

        try {
            viewMenu();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        SharedPreferences loginSharedPreferences = MainActivity.this.getSharedPreferences("loginPreferences", MODE_PRIVATE);
        boolean userLoggedStatus = loginSharedPreferences.getBoolean("isUserLogged", false);
        if (!userLoggedStatus) {
            Utils.goToActivity(MainActivity.this, LoginActivity.class);
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        try {
            viewMenu();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Displays all the courses available on the DB.
     *
     * @throws JSONException
     */
    private void viewMenu() throws JSONException {
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBarMenu);
        progressBar.setVisibility(View.VISIBLE);
        final ListView listview = findViewById(R.id.listMenu);
        final ListView panierview = findViewById(R.id.panier);
        JSONCommunication.getInstance().jsonArrayRequest(
                "courses",
                MainActivity.this,
                Request.Method.GET,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        piatti = new ArrayList<>();
                        Course piatto;
                        try {
                            for(int i=0; i<response.length(); i++) {
                                piatto = new Course(
                                        response.getJSONObject(i).getInt("id"),
                                        response.getJSONObject(i).getString("Name_course"),
                                        response.getJSONObject(i).getString("Type"),
                                        Float.valueOf(response.getJSONObject(i).getString("Price")),
                                        response.getJSONObject(i).getString("Description")
                                );
                                piatti.add(piatto);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        MenuItemAdapter adapter = new MenuItemAdapter(piatti, MainActivity.this);
                        listview.setAdapter(adapter);
                        progressBar.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Something goes wrong.", Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    }
                }
        );
        /*
        Listener that permits to add new courses into the cart or to increase the quantity.
         */
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adt, final View component, int pos, long id){
                Button b = (Button) findViewById(R.id.go_to_order);
                Course obj = (Course) adt.getItemAtPosition(pos);
                if(!panier.containsKey(obj.getName()))
                    panier.put(obj.getName(), Integer.valueOf(1));
                else
                    panier.put(obj.getName(), panier.get(obj.getName())+1);
                HashMapAdapter panier_adapter = new HashMapAdapter(panier);
                ListView panierView = findViewById(R.id.panier);
                panierView.setAdapter(panier_adapter);
                unlockGoToOrder();
            }
        });
        /*
        Listener that permits to remove courses from the cart or to decrease the quantity.
         */
        panierview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adt, final View component, int pos, long id){
                String str = String.valueOf(adt.getItemAtPosition(pos));
                String key = str.substring(0,str.indexOf('='));
                Integer value = Integer.parseInt(str.substring(key.length()+1));
                if(value > 1)
                {
                    panier.remove(key);
                    panier.put(key, value-1);
                }
                else panier.remove(key);
                HashMapAdapter panier_adapter = new HashMapAdapter(panier);
                ListView panierView = findViewById(R.id.panier);
                panierView.setAdapter(panier_adapter);
                if(panier.isEmpty()){
                    lockGoToOrder();
                }
            }
        });

        /*
        Listener that permits to remove courses from the cart.
         */
        panierview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adt, final View component, int pos, long id){
                String str = String.valueOf(adt.getItemAtPosition(pos));
                String key = str.substring(0,str.indexOf('='));
                Integer value = Integer.parseInt(str.substring(key.length()+1));
                panier.remove(key);
                HashMapAdapter panier_adapter = new HashMapAdapter(panier);
                ListView panierView = findViewById(R.id.panier);
                panierView.setAdapter(panier_adapter);
                if(panier.isEmpty()){
                    lockGoToOrder();
                }
                return true;
            }
        });
    }

    /**
     * Starts OrderActivity.
     * @param v
     */
    public void order_summary(View v){
        Intent order = new Intent(this, OrderActivity.class);
        Bundle bundle = new Bundle();
        ArrayList<Course> list = new ArrayList<>();
        for(int i = 0; i < piatti.size(); i++)
        {
            Course temp = piatti.get(i);

            if(panier.get(temp.getName()) != null)
            {
                temp.setQuantity(panier.get(temp.getName()));
                list.add(temp);
            }
        }
        bundle.putParcelableArrayList("list", list);
        order.putExtras(bundle);
        startActivity(order);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent home = new Intent(Intent.ACTION_MAIN);
            home.addCategory(Intent.CATEGORY_HOME);
            home.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(home);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_credits)
        {
            Intent lateral_page = new Intent(getApplicationContext(), LateralBarActivity.class);
            lateral_page.putExtra("choose", 4);
            startActivity(lateral_page);
        } else if (id == R.id.nav_points) {
            Intent lateral_page = new Intent(getApplicationContext(), LateralBarActivity.class);
            lateral_page.putExtra("choose", 2);
            startActivity(lateral_page);
        } else if (id == R.id.nav_profile) {
            Intent profile_page = new Intent(getApplicationContext(), ProfileActivity.class);
            profile_page.putExtra("choose", 1);
            startActivity(profile_page);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void lockGoToOrder(){
        Button b = findViewById(R.id.go_to_order);
        b.setClickable(false);
        b.setBackgroundColor(getResources().getColor(R.color.disable));
        b.setTextColor(getResources().getColor(R.color.disableText));
    }

    private void unlockGoToOrder(){
        Button b = findViewById(R.id.go_to_order);
        b.setClickable(true);
        b.setBackgroundColor(getResources().getColor(R.color.darkGreen));
        b.setTextColor(getResources().getColor(R.color.white));
    }
}