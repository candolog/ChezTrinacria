package e.alberto.cheztrinacria.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.AbstractCollection;
import java.util.ArrayList;

import e.alberto.cheztrinacria.R;
import e.alberto.cheztrinacria.data.Course;
import e.alberto.cheztrinacria.data.JSONCommunication;
import e.alberto.cheztrinacria.utils.Utils;

import static java.lang.Math.abs;

public class ConfirmOrderActivity extends AppCompatActivity {

    private ArrayList<Course> piatti;
    private String username;
    private String notes;
    private double price;
    private String address;
    private boolean flag = false;
    private int sub_points = 0;
    private int add_points = 0;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirmorder_layout);

        //Takes the courses that the user wants to order from the calling activity.
        piatti = getIntent().getParcelableArrayListExtra("list");
        //Retrieves the user ID that is making the order from the SharedPref.
        SharedPreferences loginSharedPreferences = ConfirmOrderActivity.this.getSharedPreferences("loginPreferences", MODE_PRIVATE);
        username = loginSharedPreferences.getString("username", null);
        //Retrieves the notes for the restaurant.
        notes = getIntent().getStringExtra("notes");
        //Retrieves the total price.
        price = getIntent().getDoubleExtra("total", 0);
        //Retrieves the delivery address.
        address = getIntent().getStringExtra("address");
        getPointsForUser();
    }

    private void setView(int val) throws JSONException {
        ListView summary = findViewById(R.id.summary_view);
        ListView summaryplates = findViewById(R.id.summary_plates);
        ArrayList<String> arrayList = new ArrayList<>();
        ArrayList<String> arrayList2 = new ArrayList<>();
        arrayList.add("Ordered by: " + username);
        //Adds the notes if the they have a length greater than 1, otherwise don't
        if(notes.length() > 1)
            arrayList.add("Notes: " + notes);
        /*
        Handles the discount derived from the collected points...
         */
        if(val >= 100)
        {
            flag = true;
            int discount = (val/100 < price) ? val/100 : (int) price;
            sub_points = discount*100;
            arrayList.add(String.format("Discounted price (via points): %.2f€",price));
            price = price - discount;
        }
        else arrayList.add(String.format("Total: %.2f€",price)); //...vice versa no discount applied.
        add_points = (int) price * 10;
        arrayList.add("Shipped to" + ((address.equals("-")) ? " your default address." : ": " + address));
        /*
        Inserts all the courses name in the arraylist.
         */
        for(int i = 0; i < piatti.size(); i++)
            arrayList2.add(piatti.get(i).getName());
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(ConfirmOrderActivity.this, android.R.layout.simple_list_item_1, arrayList);
        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<>(ConfirmOrderActivity.this, android.R.layout.simple_list_item_1, arrayList2);
        summary.setAdapter(arrayAdapter);
        summaryplates.setAdapter(arrayAdapter2);
    }

    /**
     * Permits the user to pay the order and to update his points.
     * @param v
     * @throws JSONException
     */
    public void pay (View v) throws JSONException {
        JSONObject orderjs = new JSONObject();
        orderjs.put("user_id", username);
        orderjs.put("Notes", notes);
        orderjs.put("total", price);
        orderjs.put("address", address);
        JSONArray piatti_ = new JSONArray();
        for(int i=0; i < piatti.size(); i++){
            JSONObject temp = new JSONObject();
            temp.put("id", piatti.get(i).getId());
            temp.put("Name_course", piatti.get(i).getName());
            temp.put("Quantity", piatti.get(i).getQuantity());
            piatti_.put(temp);
        }
        orderjs.put("Order_list", piatti_);
        JSONCommunication.getInstance().jsonObjectRequest(
                "orders/new",
                ConfirmOrderActivity.this,
                Request.Method.POST,
                orderjs,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!ConfirmOrderActivity.this.isFinishing()) {
                                if(response.getBoolean("response")) {

                                    setContentView(R.layout.finish_order);
                                    TextView point = findViewById(R.id.point);
                                    TextView data = findViewById(R.id.date);
                                    TextView restaurant = findViewById(R.id.restaurant);
                                    if(flag == true) point.setText("Points used: " + sub_points);
                                    else point.setText("Points: +" + add_points);
                                    data.setText("Date: " + response.getString("created_at"));
                                    restaurant.setText("Shipped from: " + response.getString("restaurant"));
                                }
                                else Utils.makeShortToast(ConfirmOrderActivity.this, response.getString("msg"));
                                }
                            else {
                                Utils.makeShortToast(ConfirmOrderActivity.this, response.getString("msg"));
                                }
                            }
                            catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (!ConfirmOrderActivity.this.isFinishing()) {
                            Utils.makeShortToast(ConfirmOrderActivity.this, "Check your internet connection.");
                        }
                    }
                }
        );
        setPointsAPI(- sub_points);
        SharedPreferences addr = getSharedPreferences("addr", MODE_PRIVATE);
        addr.edit().putString("address", "-").apply();
    }

    private void getPointsForUser() {
        //creating the URL for the API
        String api = "user/info/" + username;
        JSONCommunication.getInstance().jsonObjectRequest(
                api,
                ConfirmOrderActivity.this,
                Request.Method.GET,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            setView(response.getInt("Points"));
                        } catch (JSONException e) {
                            Utils.makeShortToast(ConfirmOrderActivity.this, "You must be logged in.");
                            Utils.goToActivity(ConfirmOrderActivity.this, LoginActivity.class);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ConfirmOrderActivity.this, "Failed to get the information.", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    /**
     * Updates the points of the user.
     *
     * @param points the amount of points gained or lost.
     * @throws JSONException
     */
    private void setPointsAPI(int points) throws JSONException {
        JSONObject js = new JSONObject();
        js.put("email", username);
        js.put("points", points);
        JSONCommunication.getInstance().jsonObjectRequest(
                "user/set_points/",
                ConfirmOrderActivity.this,
                Request.Method.POST,
                js,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                            if(response == null) Utils.makeShortToast(ConfirmOrderActivity.this, "Response NULL.");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ConfirmOrderActivity.this, "Check your internet connection.", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    public void go_homepage(View v){
        Utils.goToActivity(this, MainActivity.class);
        finish();
    }

    @Override
    public void onBackPressed() {
        Utils.goToActivity(this, MainActivity.class);
        finish();
    }
}