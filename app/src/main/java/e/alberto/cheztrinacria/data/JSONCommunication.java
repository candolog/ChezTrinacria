package e.alberto.cheztrinacria.data;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSONCommunication {
    private final String baseUrl = "http://trukkipcweb.altervista.org/";
    private static JSONCommunication jsonCommunication;

    private JSONCommunication() {
    }

    public static JSONCommunication getInstance() {
        if (jsonCommunication == null) {
            jsonCommunication = new JSONCommunication();
        }
        return jsonCommunication;
    }

    public void jsonObjectRequest(String url, Context context, int method, JSONObject jsonObject, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        String finalUrl = baseUrl + url;
        request(finalUrl, method, context, jsonObject, onSuccess, onError);
    }

    public void jsonArrayRequest(String url, Context context, int method, JSONObject jsonObject, Response.Listener<JSONArray> onSuccess, Response.ErrorListener onError) {
        String finalUrl = baseUrl + url;
        requestArray(finalUrl, method, context, onSuccess, onError);
    }

    private void request(String url, int method, Context context, JSONObject jsonObject, final Response.Listener<JSONObject> onSuccess, final Response.ErrorListener onError) {
        RequestQueue queue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(
                method,
                url,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        onSuccess.onResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                       onError.onErrorResponse(error);
                    }
                });
        queue.add(request);
    }

    // Usata per trattare array JSON
    private void requestArray(String url, int method, Context context, final Response.Listener<JSONArray> onSuccess, final Response.ErrorListener onError) {
        RequestQueue queue = Volley.newRequestQueue(context);
        JsonArrayRequest request = new JsonArrayRequest(
                method,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        onSuccess.onResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        onError.onErrorResponse(error);
                    }
                });
        queue.add(request);
    }
}
