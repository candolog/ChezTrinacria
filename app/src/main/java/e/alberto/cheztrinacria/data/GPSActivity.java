package e.alberto.cheztrinacria.data;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import java.util.ArrayList;

import e.alberto.cheztrinacria.R;
import e.alberto.cheztrinacria.utils.Utils;

public class GPSActivity extends AppCompatActivity {

    private ImageButton gps_ImageButton;
    private LocationManager locationManager;
    private LocationListener locationListener; //listener per la localizzazione

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gps_layout);
        gps_ImageButton = findViewById(R.id.gps_button);

        /*
        Here we assign the geolocation system service.
        The manager is also used to call requestLocationUpdates()
         */
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {

            /**
             * This method is called when the position changes compared to the previouse one.
             *
             * @param location the new location.
             */
            @Override
            public void onLocationChanged(Location location) {
                try {
                    /*
                    Here we recall the sendGPSPosition() method, which sends the coordinates to server and handles the answers.
                     */
                    sendGPSPosition(location.getLatitude(), location.getLongitude());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }

            @Override
            public void onProviderEnabled(String s) {
            }

            /**
             * Permits to automatically open the settings in order to enable the GPS (if this is disabled).
             */
            @Override
            public void onProviderDisabled(String s) {
                Utils.makeShortToast(getApplicationContext(), "You must enable GPS");
                Intent gps_setting = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(gps_setting);
            }
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{
                    /*
                    Permissions from the Manifest
                     */
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.INTERNET
            }, 99); //request code that we choose.
        }
        configuration();
    }

    /**
     * Handles the click on the geolocalization button.
     */
    private void configuration() {
        gps_ImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressBar progressBar = findViewById(R.id.progressBarAddress);
                progressBar.setVisibility(View.VISIBLE);
                /*
                requestLocationUpdates() asks for a provider , a minimum update period,
                 a minimum update distance and a LocationListener
                 */
                locationManager.requestLocationUpdates("gps", 15000, 100, locationListener);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //switch per le richieste, noi ne abbiamo solo una.
        /*
        switch block for the responses, now we're handling just one.
         */
        switch (requestCode) {
            /*
            Request 99 is the one that we choose previously (see row 97).
            We call the method configuration() only if the permissions are granted.
             */
            case 99:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    configuration();
                return;
        }
    }

    /**
     * It sends to the server the GPS position in order to retrieve the closer addresses.
     *
     * @param x double value for the latitude.
     * @param y double value for the longitude.
     * @throws JSONException
     */
    private void sendGPSPosition(double x, double y) throws JSONException {
        final ProgressBar progressBar = findViewById(R.id.progressBarAddress);
        final ListView addressview = findViewById(R.id.view_address);
        final ArrayList<String> list_address = new ArrayList<>();
        JSONCommunication.getInstance().jsonArrayRequest(
                "geo/GPSToAddress/" + x + "-" + y,
                GPSActivity.this,
                Request.Method.GET,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            if (response.length() == 0)
                                Utils.makeShortToast(GPSActivity.this, "This app works in the urban area of Paris. Enter the address manually.");
                            else {
                                for (int i = 0; i < response.length(); i++)
                                    list_address.add(response.getJSONObject(i).getString("street"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressBar.setVisibility(View.GONE);
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(GPSActivity.this, android.R.layout.simple_list_item_1, list_address);
                        addressview.setAdapter(adapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        Utils.makeShortToast(GPSActivity.this, "Something goes wrong.");
                    }
                }
        );
        addressview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adt, final View component, int pos, long id) {
                Utils.setSharedAddress(GPSActivity.this, adt, pos);
                closeGPS();
            }
        });
    }

    /**
     * Closes the GPS.
     */
    private void closeGPS() {
        if(locationManager != null){
            locationManager.removeUpdates(locationListener);
            locationManager = null;
        }
        if(locationListener != null)
            locationListener = null;
        finish();
    }

    /**
     * Sends a String in order to retrieve in the DB the closest addresses in terms of characters.
     *
     * @param v
     * @throws JSONException
     */
    public void sendAddress(View v) throws JSONException {
        final ProgressBar progressBar = findViewById(R.id.progressBarAddress);
        final ListView addressview = findViewById(R.id.view_address);
        final ArrayList<String> list_address = new ArrayList<>();
        progressBar.setVisibility(View.VISIBLE);
        EditText addressedittext = findViewById(R.id.address_gps);
        JSONCommunication.getInstance().jsonArrayRequest(
                "geo/" + addressedittext.getText(),
                GPSActivity.this,
                Request.Method.GET,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            if (response.length() == 0)
                                Utils.makeShortToast(GPSActivity.this, "This app works in the urban area of Paris. Enter another address.");
                            else {
                                for (int i = 0; i < response.length(); i++)
                                    list_address.add(response.getJSONObject(i).getString("street"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressBar.setVisibility(View.GONE);
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(GPSActivity.this, android.R.layout.simple_list_item_1, list_address);
                        addressview.setAdapter(adapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        Utils.makeShortToast(GPSActivity.this, "Something goes wrong.");
                    }
                }
        );
        addressview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adt, final View component, int pos, long id) {
                Utils.setSharedAddress(GPSActivity.this, adt, pos);
                closeGPS();
            }
        });
    }

    @Override
    public void onBackPressed(){
        closeGPS();
    }

    @Override
    public void onResume() {
        super.onResume();
        ProgressBar progressBar = findViewById(R.id.progressBarAddress);
        progressBar.setVisibility(View.GONE);
    }
}