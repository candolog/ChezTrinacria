package e.alberto.cheztrinacria.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import e.alberto.cheztrinacria.R;
import e.alberto.cheztrinacria.data.Course;

public class MenuItemAdapter extends ArrayAdapter<Course>{

    private ArrayList<Course> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtPrice;
    }

    public MenuItemAdapter(ArrayList<Course> data, Context context) {
        super(context, R.layout.row_item_menu, data);
        this.dataSet = data;
        this.mContext=context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Course dataModel = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item_menu, parent, false);
            viewHolder.txtName = convertView.findViewById(R.id.name);
            viewHolder.txtPrice = convertView.findViewById(R.id.price);
            convertView.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.txtName.setText(dataModel.getName());
        viewHolder.txtPrice.setText(String.format("%.2f",dataModel.getPrice()) + "€");

        return convertView;
    }

}