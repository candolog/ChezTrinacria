package e.alberto.cheztrinacria.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import e.alberto.cheztrinacria.R;
import e.alberto.cheztrinacria.activities.LoginActivity;
import e.alberto.cheztrinacria.activities.ProfileActivity;
import e.alberto.cheztrinacria.data.JSONCommunication;

import static android.content.Context.MODE_PRIVATE;

public class Utils {

    public static void makeShortToast(Context context, String msg){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static boolean isValidEmail(CharSequence mail) {
        return mail != null && android.util.Patterns.EMAIL_ADDRESS.matcher(mail).matches();
    }

    public static void goToActivity(Context context, Class c)
    {
        Intent activity = new Intent(context, c);
        context.startActivity(activity);
    }

    public static void setSharedAddress(Context context, AdapterView<?> adt, int pos){
        String obj = (String) adt.getItemAtPosition(pos);
        SharedPreferences addr = context.getSharedPreferences("addr", MODE_PRIVATE);
        addr.edit().putString("address", obj).apply();
    }

    public static void getSharedAddress(Context context){
        SharedPreferences addr = context.getSharedPreferences("addr", MODE_PRIVATE);
        addr.edit().putString("address", "").apply();
    }
}