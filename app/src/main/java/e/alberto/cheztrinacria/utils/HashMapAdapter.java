package e.alberto.cheztrinacria.utils;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Map;
import e.alberto.cheztrinacria.R;

import static android.media.CamcorderProfile.get;

public class HashMapAdapter<E,T> extends BaseAdapter {
    private final ArrayList mData;

    public HashMapAdapter(Map<E, T> map) {
        mData = new ArrayList();
        mData.addAll(map.entrySet());
    }

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtQuantity;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Map.Entry<E, T> getItem(int position) {
        return (Map.Entry) mData.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Map.Entry<E, T> dataModel = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.row_item_menu, parent, false);
            viewHolder.txtName = convertView.findViewById(R.id.name);
            viewHolder.txtQuantity = convertView.findViewById(R.id.price);
            convertView.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.txtName.setText(dataModel.getKey().toString());
        viewHolder.txtQuantity.setText("x " + dataModel.getValue().toString());

        return convertView;
    }
}